export default [
    {
        path: '/antadmin/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '@/views/admin/Login.vue'),
    },
    {
        path: '/antadmin',
        name: 'antadmin',
        component: () => import(/* webpackChunkName: "about" */ '@/views/admin/Index.vue'),
        redirect: '/antadmin/dashboard',
        children:[
            // 后台首页
            {
                path: 'dashboard',
                name: 'dashboard',
                component: () => import(/* webpackChunkName: "about" */ '@/views/admin/dashboard/Index.vue')
            },
        ]
    },
]