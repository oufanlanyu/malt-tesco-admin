import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		// 左侧菜单是否收缩
		toggler: true,
		// 子菜单是否收缩
		childmenutoggler: true,
		// 控制右侧消息栏是否收缩
		msgtoggler: true
	},
	mutations: {
		// 控制左侧菜单
		isLeftMenu(state, data) {
			state.toggler = !state.toggler
		},
		// 控制子菜单
		isLeftChildMenu(state, data) {
			state.childmenutoggler = !state.childmenutoggler
		},
		// 控制右侧消息栏
		isMsg(state, data) {
			state.msgtoggler = !state.msgtoggler
		},
	},
	actions: {
	},
	modules: {
	}
})
