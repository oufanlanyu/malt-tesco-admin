export default [
    {
        path: '/',
        name: 'Index',
        component: () => import(/* webpackChunkName: "about" */ '@/views/Index.vue')
    },
]