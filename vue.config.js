const webpack = require('webpack');
const path = require('path');
module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/theme.scss";`,
            },
        }
    },
    lintOnSave: false,
    devServer: {
        // https: true,
        port: 1001,
        proxy: {
            '/jeecg-boot': {
                target: 'http://localhost:8086',
                // target: 'http://192.168.1.72:8080',
                ws: false,
                changeOrigin: true
            }
        }
    }
}
