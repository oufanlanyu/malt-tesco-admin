import Vue from 'vue'
import VueRouter from 'vue-router'
import layout from './layout'
import main from './admin/main'

Vue.use(VueRouter)
let routes=new Set([...layout,...main])
// routes.add({
//   path: '*',
//   component: () =>
//       import ('@/views/404.vue')
// })


const router=new VueRouter({
	routes,
	mode: 'history'
})

router.beforeEach((to,from,next) => {
	document.documentElement.scrollTop=0
	// let token=sessionStorage.getItem('token')
	// if(!token) {
	// 	if(to.path!=='/login') {
	// 		return next('/login')
	// 	}
	// } else {
	// 	if(to.path=='/login') {
	// 		router.go(-1)
	// 	}
	// }
	next()
});

export default router
